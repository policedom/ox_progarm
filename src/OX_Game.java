import java.util.Scanner;

public class OX_Game {
	  private static int row,col;
	  private static int empty = 0;
	  private static char[][] boardOX = new char[3][3];
	  private static Scanner kb = new Scanner(System.in);
	  private static char turn = 'X';
	  private static String win_result = "default";
	  private static int draw = 0;

	  
   public static void main(String[] args) {
	   Showwelcome();
	   
	   for(int i=0; i<3; i++) {
		   for(int j=0; j<3; j++) {
			   boardOX[i][j] = '-';
		   }
	   }
	   Showboard();
	   Boardplay();
	 
   }
   
   public static void Showwelcome() {
	   System.out.println("Welcome to OX game");
	   System.out.println("__________________");
   }
   
   public static void Boardplay() {
	   boolean play = true;
	   while (play) {
	     try {
		   System.out.println("Turn "+ turn);
		   System.out.println("Input Row Col : ");
		   row = kb.nextInt() - 1;
		   col = kb.nextInt() - 1;
		   boardOX [row][col] = turn;
		   draw++;
		   
		   
		   if(Checkboard(row,col)) {
			   play = false;
			   Result(); 
			 }
		   
		   Showboard();	
		   
		   if (turn == 'X') {   
			   turn = 'O';
		   }else {
			   turn = 'X';		       
		   }
	  
	  
	     }catch(Exception e) {
	    	 System.out.println("Error Please input correct row col ");
			   continue;
	     }
	   }
   }
   
   public static void Showboard() {
	   for(int i=0; i<3; i++) {
		   System.out.println();
		   for(int j=0; j<3; j++) {
			   System.out.print(boardOX[i][j] + " ");
			 
		   }
	   }
	   System.out.println();
	   System.out.println("__________________");
   }
   public static void input() {
	   
   }
 
   public static boolean Checkboard(int C_row,int C_col) {
	   // ���� �ǵ������ǹ͹
	   if(boardOX[0][C_col] == boardOX[1][C_col] && boardOX[0][C_col] == boardOX[2][C_col]) {
		   win_result = "Win as Vertical";
		   return true;
	   }
	   if(boardOX[C_row][0] == boardOX[C_row][1] && boardOX[C_row][0] == boardOX[C_row][2]) {
		   win_result = "Win as Landscape";
		   return true;
	   }   
	   // ���Ƿ�§
	   if(boardOX[0][0] == boardOX[1][1] && boardOX[0][0] == boardOX[2][2] && boardOX[1][1]!= '-' ) {
		   win_result = "Win as Diagonal";
		   return true;
	   }
	   if(boardOX[0][2] == boardOX[1][1] && boardOX[0][2] == boardOX[2][0] && boardOX[1][1]!= '-' ) {
		   win_result = "Win as Diagonal";
		   return true;
	   }
	   if(boardOX[0][0] == boardOX[1][1] && boardOX[0][0] == boardOX[2][2] && boardOX[1][1]!= '-' ) {
		   win_result = "Win as Diagonal";
		   return true;   
	   }
	   if(isDraw()) {
		   return true;   
	   }
	   return false;
   }

   public static boolean isDraw() {
	   if(draw==9) return true;
	   return false;
		   
	   }
   
   public static void Result() {
	   if(isDraw()) {
	   System.out.println("Draw!!!!");
	   System.out.println("------------------------------");
	   System.out.println("Result");
	   }else {
	   System.out.println("------------------------------");
	   System.out.println("Player " +turn+" "+ win_result );
	   System.out.println("------------------------------");
	   System.out.println("Result");
	   }
   }
   
   

}
